package com.imc;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Main extends Activity {
	protected void onCreate(Bundle instance) {
		super.onCreate(instance);
		setContentView(R.layout.main);
		
		final Button btnCalcular = (Button) findViewById(R.id.btnCalcular);
		final EditText txtPeso = (EditText) findViewById(R.id.txtPeso);
		final EditText txtAltura = (EditText) findViewById(R.id.txtAltura);
		final TextView lblResult = (TextView) findViewById(R.id.lblResult);
		final TextView lblResultDescription = (TextView) findViewById(R.id.lblResultDescription);
		final LinearLayout viewResult = (LinearLayout) findViewById(R.id.viewResult);
		final ImageView imgPointer = (ImageView) findViewById(R.id.imgPointer);
		
		btnCalcular.setOnClickListener(new Button.OnClickListener() {
			@SuppressWarnings("deprecation")
			@Override
			public void onClick(View v) {
				try {
					float peso = Float.parseFloat(txtPeso.getText().toString());
					float altura = Float.parseFloat(txtAltura.getText().toString());
					
					if (peso == 0 || altura == 0) {
						handleError();
						return ;
					}
					
					float imc = peso / (altura * altura);
					String peso_ideal_i = String.format("%.2f", 18.5F * (altura * altura));
					String peso_ideal_f = String.format("%.2f", 24.99F * (altura * altura));
					
					lblResult.setText(String.format("%.2f", imc));
					if (imc < 16F) {
						lblResultDescription.setText("Muito abaixo do peso! Você apresenta Magreza grave. O peso ideal para a sua estatura está entre "+ peso_ideal_i +"(kg) e "+ peso_ideal_f +"(kg).");
						imgPointer.setImageResource(R.drawable.pointer1);
					} else if (imc < 17F) {
						lblResultDescription.setText("Atenção! Você apresenta Magreza moderada. O peso ideal para a sua estatura está entre "+ peso_ideal_i +"(kg) e "+ peso_ideal_f +"(kg).");
						imgPointer.setImageResource(R.drawable.pointer2);
					} else if (imc < 18.5F) {
						lblResultDescription.setText("Abaixo do peso. Você apresenta Magreza leve. O peso ideal para a sua estatura está entre "+ peso_ideal_i +"(kg) e "+ peso_ideal_f +"(kg).");
						imgPointer.setImageResource(R.drawable.pointer3);
					} else if (imc < 25F) {
						lblResultDescription.setText("Peso normal. Parabéns e mantenha-se saudável! O peso ideal para a sua estatura está entre "+ peso_ideal_i +"(kg) e "+ peso_ideal_f +"(kg).");
						imgPointer.setImageResource(R.drawable.pointer4);
					} else if (imc < 30F) {
						lblResultDescription.setText("Atenção! Você está acima do peso. O peso ideal para a sua estatura está entre "+ peso_ideal_i +"(kg) e "+ peso_ideal_f +"(kg).");
						imgPointer.setImageResource(R.drawable.pointer5);
					} else if (imc < 35F) {
						lblResultDescription.setText("Atenção! Você apresenta Obesidade grau I. O peso ideal para a sua estatura está entre "+ peso_ideal_i +"(kg) e "+ peso_ideal_f +"(kg).");
						imgPointer.setImageResource(R.drawable.pointer6);
					} else if (imc < 40F) {
						lblResultDescription.setText("Atenção! Você apresenta Obesidade grau II (severa). O peso ideal para a sua estatura está entre "+ peso_ideal_i +"(kg) e "+ peso_ideal_f +"(kg).");
						imgPointer.setImageResource(R.drawable.pointer7);
					} else {
						lblResultDescription.setText("Atenção! Você apresenta Obesidade grau III (mórbida). O peso ideal para a sua estatura está entre "+ peso_ideal_i +"(kg) e "+ peso_ideal_f +"(kg).");
						imgPointer.setImageResource(R.drawable.pointer8);
					}
				} catch (NumberFormatException e) {
					handleError();
				}
			}
		});
	}
	
	protected void handleError() {
		AlertDialog alert = new AlertDialog.Builder(Main.this).create();
		alert.setTitle("Erro");
		alert.setMessage("Os campos Peso e Altura são de preenchimento obrigatório.");
		alert.setCanceledOnTouchOutside(true);
		alert.setButton("OK", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		});
		alert.show();
	}
}
